import 'package:flutter/material.dart';

const kTempTextStyle = TextStyle(
  fontFamily: 'LobsterTwo',
  fontSize: 100.0,
);

const kMessageTextStyle = TextStyle(
  fontFamily: 'LobsterTwo',
  fontSize: 60.0,
);

const kButtonTextStyle = TextStyle(
  fontFamily: 'LobsterTwo',
  fontSize: 30.0,
);

const kConditionTextStyle = TextStyle(
  fontFamily: 'LobsterTwo',
  fontSize: 100.0,
);
const kTextFieldInputDecoration = InputDecoration(
    filled: true,
    fillColor: Colors.white,
    icon: Icon(
      Icons.location_city,
      color: Colors.white,
    ),
    hintText: 'Entrer City Name',
    hintStyle: TextStyle(
      color: Colors.grey,
    ),
    border: OutlineInputBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(15.0),
        ),
        borderSide: BorderSide.none));
